import { Injectable } from '@angular/core';
import { Country } from './country.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CountryService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  private countries: Country[] = [];

  constructor(private http: HttpClient) {}

  getCountries() {
    return this.countries.slice();
  }

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<Country[]>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .subscribe(countries => {
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, () => {
        this.countriesFetching.next(false);
      });
  }

  fetchCountry(id: string) {
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${id}`).pipe(
      map(result => {
        return new Country(
          result.name,
          id,
          result.capital,
          `http://146.185.154.90:8080/restcountries/data/${id.toLowerCase()}.svg`,
          result.area,
          result.population
        );
      }),
    );
  }

}
