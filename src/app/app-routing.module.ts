import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailsComponent } from './countries/country-details/country-details.component';
import { CountryResolverService } from './countries/country-resolver.service';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: '', component: AppComponent},
  {
    path: 'country/:code',
    component: CountryDetailsComponent,
    resolve: {
      country: CountryResolverService,
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
