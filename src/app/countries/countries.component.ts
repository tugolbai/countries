import { Component, OnInit } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { Subscription } from 'rxjs';
import { Country } from '../shared/country.model';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  countries: Country[] = [];
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private countryService: CountryService) { }

  ngOnInit()  {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countriesChange.subscribe((countries: Country[]) => {
      this.countries = countries;
      console.log(this.countries);
    });
    this.countriesFetchingSubscription = this.countryService.countriesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.countryService.fetchCountries();
  }
}
